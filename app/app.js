(function () {
    angular
        .module("indexeddb-proto", [
            "LocalForageModule",
            "ui.router"
        ])
        .config(function ($stateProvider, $urlRouterProvider, $localForageProvider){
            ConfigureRoutes($stateProvider, $urlRouterProvider);
            ConfigureStorage($localForageProvider);
        });

    function ConfigureRoutes($stateProvider, $urlRouterProvider){
        $stateProvider
            .state({
                name: "read",
                url: "/read",
                templateUrl: "app/read/read-view.html"
            })
            .state({
                name: "write",
                url: "/write",
                templateUrl: "app/write/write-view.html"
            });

        $urlRouterProvider.otherwise("/");
    }

    function ConfigureStorage($localForageProvider){
        $localForageProvider.config({
            driver: $localForageProvider.INDEXEDDB
        });
    }
})();