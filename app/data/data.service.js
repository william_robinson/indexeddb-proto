(function () {
    angular
        .module("indexeddb-proto")
        .factory("DataSvc", DataSvc);

    DataSvc.$inject = ["$localForage"];
    function DataSvc($localForage){
        return {
            write: Write,
            read: Read,
            getKeys: GetKeys,
            clear: Clear,
            writeList: WriteList
        };

        function Write(key, value){
            return $localForage.setItem(key, value);
        }

        function Read(key){
            return $localForage.getItem(key)
                .then(function(result){
                    return result;
                });
        }

        function GetKeys(){
            var keys = [];
            return $localForage.iterate(function(value, key){
                    keys.push(key);
                })
                .then(function(){
                    return keys;
                });
        }

        function Clear(){
            return $localForage.clear();
        }

        function WriteList(keys, values){
            return $localForage.setItem(keys, values);
        }
    }
})();