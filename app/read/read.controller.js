(function () {
    angular
        .module("indexeddb-proto")
        .controller("ReadCtrl", ReadCtrl);

    ReadCtrl.$inject = ["DataSvc"];
    function ReadCtrl(DataSvc){
        var ctrl = this;

        ctrl.Keys = [];
        ctrl.Read = Read;

        ReadKeys();

        function Read(key){
            return DataSvc.read(key)
                .then(function(result){
                    return result;
                })
        }

        function ReadKeys(){
            DataSvc.getKeys()
                .then(function(result) {
                    ctrl.Keys = result;
                });
        }
    }
})();