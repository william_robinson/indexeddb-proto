(function () {
    angular
        .module("indexeddb-proto")
        .directive("readData", ReadDataDirective);

    ReadDataDirective.$inject = ["DataSvc"];
    function ReadDataDirective(DataSvc){
        return {
            restrict: "E",
            scope: {},
            link: function(scope, ele, attr){
                attr.$observe("key", function(newKey){
                    DataSvc.read(newKey)
                        .then(function(result){
                           scope.Value = result;
                        });
                });
            },
            template:"<strong>{{ Value }}</strong>"
        };
    }
})();