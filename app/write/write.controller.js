(function () {
    angular
        .module("indexeddb-proto")
        .controller("WriteCtrl", WriteCtrl);

    WriteCtrl.$inject = ["$q", "$filter", "DataSvc"];
    function WriteCtrl($q, $filter, DataSvc){
        var ctrl = this;
        var lastKeyNumber = 0;

        ctrl.Messages = ["Waiting for new value..."];
        ctrl.IsClearConfirmed = true;
        ctrl.Times = 1;
        ctrl.Size = 100;
        ctrl.IsWriting = false;
        ctrl.Writes = 1
        ctrl.Write = Write;
        ctrl.Clear = Clear;
        ctrl.CancelClear = CancelClear;
        ctrl.MassWrite = MassWrite;

        function Write(key, value){
            ctrl.IsWriting = true;
            ctrl.Messages.push("Writing: " + key);
            DataSvc.write(key, value)
                .then(function(){
                    ctrl.Messages.push("Finished writing: " + key);
                    ctrl.InputKey = "";
                    ctrl.InputValue = "";
                    ctrl.IsWriting = false;
                });
        }

        function Clear(){
            if(ctrl.IsClearConfirmed){
                ctrl.IsClearConfirmed = false;
            } else{
                ctrl.IsClearConfirmed = true;
                DataSvc.clear()
                    .then(function(){
                        lastKeyNumber = 0;
                        AddMessage("Data cleared!");
                    });
            }
        }

        function CancelClear(){
            ctrl.IsClearConfirmed = true;
        }

        function AddMessage(msg){
            ctrl.Messages.push(msg);
        }

        function MassWrite(){
            var intTimes = parseInt(ctrl.Times);
            if(!intTimes){
                AddMessage("Set the number of items to add.");
            }
            var intSize = parseInt(ctrl.Size);
            if(!intSize){
                AddMessage("Set the number of characters in each item to add.");
            }
            var intWrites = parseInt(ctrl.Writes);
            if(!intWrites){
                AddMessage("Set the number of times each set should be written.");
            }

            ctrl.IsWriting = true;
            AddMessage("Beginning mass write at " + $filter("date")(new Date(), "hh:mm:ss:sss") + "...");

            var newItem = "";
            for (var j = 0; j < intSize; j++){
                newItem = newItem + "0";
            }
            DoWrite(newItem, intTimes, intSize, intWrites, 0)
        }

        function DoWrite(newItem, intTimes, intSize, intWrites, iteration){
            AddMessage("Building list of " + intTimes + " items of " + intSize + " characters...")
            var keys = [];
            var items = [];
            for (var i = 0; i < intTimes; i++){
                ++lastKeyNumber;
                keys.push("key-" + lastKeyNumber);
                items.push(newItem);
            }
            AddMessage("List " + (iteration + 1) + " completed.");
            AddMessage("Writing list " + (iteration + 1) + " to storage...");
            DataSvc.writeList(keys, items).then(function(){
                var x = iteration + 1;
                if(x < intWrites){
                    DoWrite(newItem, intTimes, intSize, intWrites, x);
                } else {
                    AddMessage("Writing completed at " + $filter("date")(new Date(), "hh:mm:ss:sss") + ".");
                    ctrl.IsWriting = false;
                }
            });
        }
    }
})();