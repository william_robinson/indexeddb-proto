(function () {

    angular
        .module("indexeddb-proto")
        .controller("HomeCtrl", HomeCtrl);

    function HomeCtrl(){
        var ctrl = this;
        ctrl.Title = "Angular Seed";
        ctrl.Author = "William Robinson";
    }

})();