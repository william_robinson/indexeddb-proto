# indexeddb-proto #

This is a simple web app for experimenting with the angular-localForage library and determining the best way to use it and indexedDB with our app.

### How do I get set up? ###

* close this repo
* run **npm install**
* run **bower install**
* start an http server (e.g. node's **http-server**)

### Who do I talk to? ###

* If you have any questions or suggestions, email me: w.robinson@nextech.com